#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup


setup(
	name="reemul",
	version="v0.1-DEV",
	description="Migration manager for PostgreSQL",
	long_description="""Reemul is a migration manager for PostgreSQL""",
	author="Federico Campoli",
	author_email="thedoctor@pgdba.org",
	platforms=[
		"linux"
	],
	classifiers=[
		"License :: OSI Approved :: BSD License",
		"Environment :: Console",
		"Intended Audience :: Developers",
		"Intended Audience :: Information Technology",
		"Intended Audience :: System Administrators",
		"Natural Language :: English",
		"Operating System :: POSIX :: BSD",
		"Operating System :: POSIX :: Linux",
		"Programming Language :: Python",
		"Programming Language :: Python :: 3",
		"Programming Language :: Python :: 3.5",
		"Programming Language :: Python :: 3.6",
		"Topic :: Database :: Database Engines/Servers",
		"Topic :: Other/Nonlisted Topic"
	],
)
